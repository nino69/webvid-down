![](https://img.shields.io/badge/platform-win%20%7C%20linux%20%7C%20osx-brightgreen) ![](https://img.shields.io/badge/python-%3E=%203.5.0-orange)

🚀 Pengunduh video, digunakan untuk mengunduh video yang dapat diputar secara online dari situs web.

---

| 站点                                | URL                                                    | 普通画质 | VIP专属 |
| ------------------------------------- | ------------------------------------------------------ | -------- | ------- |
| 哔哩哔哩（单P/多P）                   | [https://www.bilibili.com/](https://www.bilibili.com/) | ✓        | ✓       |
| 爱奇艺     | [https://www.iqiyi.com/](https://www.iqiyi.com/)                       | ✓        | ✓       |
| 腾讯视频         | [https://v.qq.com/](https://v.qq.com/)                          | ✓        | ✓       |
| 芒果TV       | [https://www.mgtv.com/](https://www.mgtv.com/)                        | ✓        | ✓       |
| WeTV             | [https://wetv.vip/](https://wetv.vip/)                              | ✓        | ✓ |
| 爱奇艺国际站   | [https://www.iq.com/](https://www.iq.com/)                    | ✓        | ✓       |

